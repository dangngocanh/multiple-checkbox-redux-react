import React, { Component } from "react";
import { connect } from "react-redux";
class Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: []
    };
  }

  componentDidMount() {
    //console.log(this.props);
  }
  componentWillReceiveProps(nextProps) {
    //console.log("ok", nextProps);
    var arr = nextProps.data.checklists.data.map(e => {
      return (
        <div>
          <strong>{e}</strong>
        </div>
      );
    });
    //console.log(arr);
    this.setState({
      arr: arr
    });
  }

  render() {
    return <div className="resultBox">{this.state.arr}</div>;
  }
}

const mapStateToProps = data => {
  return {
    data: data
  };
};
export default connect(
  mapStateToProps,
  null
)(Result);
