import React, { Component } from "react";
import { connect } from "react-redux";
import "./App.css";
import { actSelectData } from "./actions";
import Result from "./components/Result";

class App extends Component {
  constructor() {
    super();
    this.state = {
      arr: [],
      search: "",
      data: [
        {
          name: "Apple",
          show: false
        },
        {
          name: "SamSung",
          show: false
        },
        {
          name: "HTC",
          show: false
        },
        {
          name: "Sony",
          show: false
        }
      ]
    };
  }
  onChange = event => {
    //const name = event.target.name;
    const value = event.target.value;
    const check = event.target.checked;
    const values = this.state.arr;
    if (check === true) {
      values.push(value);
    } else {
      for (var i = 0; i < values.length; i++) {
        if (values[i] === value) {
          values.splice(i, 1);
        }
      }
    }
    this.props.onChangeData(values);
  };

  onReset = () => {
    const checkboxes = document.getElementsByClassName("checked");
    for (let i = 0; i < checkboxes.length; i++) {
      checkboxes[i].checked = false;
    }
    this.props.onChangeData([]);
  };
  //--------------------
  onSearch = event => {
    var { value } = event.target;
    //value.toLowerCase()
    //console.log(value.toLowerCase())
    var { data } = this.state;
    data.forEach(e => {
      if (e.name.toLowerCase().indexOf(value.toLowerCase()) > -1) {
        e.show = false;
      } else e.show = true;
    });
    this.setState({});
    value = value.toLowerCase();
  };

  componentWillReceiveProps(nextProp) {
    this.setState({
      arr: nextProp.data.checklists.data
    });
  }

  render() {
    const data = this.state.data;

    const showlist = data.map((e, index) => {
      return (
        <li key={index} hidden={e.show}>
          <label>
            <input
              className="checked"
              name="name"
              value={e.name}
              type="checkbox"
              onChange={this.onChange}
            />
            {e.name}
          </label>
        </li>
      );
    });

    return (
      <div className="section">
        <from className="main">
          <div className="dropDown">
            <input
              name="search"
              type="text"
              placeholder="nhập tìm kiếm"
              className="search"
              onChange={this.onSearch}
            />
            <div className="form-check">
              <ul id="listData">{showlist}</ul>
            </div>
            <div className="result">
              <p className="show-result">
                {/* Result :
                 {this.state && this.state.arr.length === 0
                  ? null
                  : this.state.arr.map(e => {
                      return (
                        <div>
                          <strong>{e}</strong>
                        </div>
                      );
                    })} */}
              </p>
              <button onClick={this.onReset}>Reset</button>
            </div>
          </div>
          <Result />
        </from>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onChangeData: data => {
      dispatch(actSelectData(data));
    }
  };
};
const mapStatetoProps = data => {
  return {
    data: data
  };
};
export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(App);
