import * as Types from "../constants/Actiontype";

export const actSelectData = data => {
  return {
    type: Types.ADD_CHECKLIST,
    data
  };
};
