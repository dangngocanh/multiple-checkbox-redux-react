import * as Types from "../constants/Actiontype";
const initialState = [];

const checklists = (state = initialState, action) => {
  switch (action.type) {
    case Types.ADD_CHECKLIST:
      state.data = action.data;
      return { ...state };
    default:
      return { ...state };
  }
};

export default checklists;
